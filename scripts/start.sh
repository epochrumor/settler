#!/bin/sh

set -e

NAME=settler

docker stop $NAME || true
docker rm $NAME || true

docker run \
  -d \
  --name=$NAME \
  -v ${PWD}/keys:/src/keys \
  -v ${PWD}/.env:/src/.env \
  $(docker build . -q)
