require('dotenv').config()
const Web3 = require('web3')
const BN = require('bn.js')
const fs = require('fs')
const openpgp = require('openpgp')
const crypto = require('crypto')
const secp256k1 = require('secp256k1')
const { sharedSecret, fromBytes, toBytes } = require('./shared_secret')
const axios = require('axios')
const path = require('path')
const readline = require('readline')
const { Writable } = require('stream')

const {
  API_URL,
  USERNAME,
  PASSWORD,
  MASTER_PASSWORD,
  GETH_URL,
} = process.env

axios.defaults.baseURL = API_URL || 'http://localhost:4000'

// Keep tokens for 1 day
const TOKEN_TTL = 1000 * 60 * 60 * 24
let _activeToken
let _tokenExpiration
async function getToken() {
  if (_tokenExpiration && +(new Date()) > _tokenExpiration) {
    _tokenExpiration = null
    _activeToken = null
    return await getToken()
  }
  if (_activeToken) {
    return _activeToken
  }
  const { data: auth } = await axios.post('/users/login', {
    username: USERNAME,
    password: PASSWORD,
  })
  _tokenExpiration = +(new Date()) + TOKEN_TTL
  _activeToken = auth.token
  return _activeToken
}

// Main run loop
;(async () => {
  const keyPath = path.join(__dirname, 'keys')
  const contents = fs.readdirSync(keyPath)
  const publicKeyPaths = contents.filter(filename => filename.indexOf('.pub') !== -1)
  const privateKeyPath = contents.find(filename => filename.indexOf('.key') !== -1)

  // Load all public keys from .pub files
  const publicKeys = (await Promise.all(
    publicKeyPaths.map(async keyname => {
      const filepath = path.join(keyPath, keyname)
      const publicKeyArmored = fs.readFileSync(filepath).toString()
      const { keys } = await openpgp.key.readArmored(publicKeyArmored)
      return keys
    })
  )).flat()
  if (publicKeys.length === 0) {
    process.exit(1)
  }
  if (!privateKeyPath) [
    process.exit(1)
  ]

  const privateKeyArmored = fs.readFileSync(path.join(keyPath, privateKeyPath))
  const { keys: [privateKey] } = await openpgp.key.readArmored(privateKeyArmored)
  try {
    let password = MASTER_PASSWORD
    if (!password) {
      password = await readPassword(`Enter the password for "${privateKeyPath}":\n`)
    }
    await privateKey.decrypt(password)
  } catch (err) {
    console.log('Bad master password')
    process.exit(1)
  }
  console.log('Unlocked')

  for (;;) {
    try {
      await createAccounts(privateKey, publicKeys)
      await createTransactions(privateKey, publicKeys)
    } catch (err) {
      if (err.response) {
        const { status, data } = err.response
        console.log(`Error: ${status}`)
        console.log(`Message: ${data.message}`)
      } else {
        console.log('Unknown error creating accounts')
        console.log(err)
      }
    } finally {
      await new Promise(r => setTimeout(r, 5 * 1000))
    }
  }
})()

async function createTransactions(privateKey, publicKeys) {
  const token = await getToken()
  const { data: transactions } = await axios.post('/get/accounts/transactions', {
    token,
  })
  const txPromises = []
  for (const tx of transactions) {
    // Broadcast the transactions and update state
    const { data: ethPrivateKey } = await openpgp.decrypt({
      message: await openpgp.message.readArmored(tx.sender.privateKeyEncrypted),
      publicKeys,
      privateKeys: [ privateKey ],
    })
    txPromises.push(broadcastTransaction(tx, ethPrivateKey))
  }
  try {
    await Promise.all(txPromises)
  } catch (err) {
    console.log('Error with broadcasted transaction')
    console.log(err)
  }
}

/**
 * Take the decrypted private key and broadcast a transaction and wait for block receipts
 */
async function broadcastTransaction(transaction, privateKeyDecrypted) {
  const web3 = new Web3(GETH_URL)
  let confirmationCount = 0
  const { rawTransaction } = await web3.eth.accounts.signTransaction({
    from: transaction.from,
    to: transaction.to,
    value: transaction.amount,
    gas: transaction.gas,
    gasPrice: transaction.gasPrice,
  }, privateKeyDecrypted)
  console.log(`
[${(new Date()).toISOString()}] Broadcasting transaction
${parseFloat(web3.utils.fromWei(new BN(transaction.amount))).toFixed(5)} Eth
${transaction.from.slice(0,8)} -> ${transaction.to.slice(0,8)}
Account ID: ${transaction.accountId}
Type: ${transaction.type}
`)
  await new Promise((rs, rj) => {
    web3.eth.sendSignedTransaction(rawTransaction)
      // Wait for 3 confirms
      .on('confirmation', () => {
        if (++confirmationCount > 3) {
          rs()
        }
      })
  })
}

/**
 * Idempotently creates accounts for users
 **/
async function createAccounts(pgpPrivateKey, pgpPublicKeys) {
  const token = await getToken()
  // Find users for which no Account document exists
  const { data: userIds } = await axios.post('/get/accounts/needed', {
    token,
  })
  if (userIds.length === 0) {
    return
  }
  const accounts = []
  for (const _id of userIds) {
    // Create account
    const wallet = getWallet()
    const { data } = await openpgp.encrypt({
      message: openpgp.message.fromText(wallet.privateKey),
      publicKeys: pgpPublicKeys,
      privateKeys: [pgpPrivateKey],
    })
    const privateKeyEncrypted = await openpgp.stream.readToEnd(data)
    accounts.push({
      token: token,
      address: wallet.address,
      publicKey: wallet.publicKey,
      privateKeyEncrypted,
      ownerId: _id,
    })
  }
  await axios.post('/accounts/create', {
    accounts,
    token,
  })
}

// Safely read password from terminal
async function readPassword(prompt = '') {
  const mutableStdout = new Writable({
    write: function (chunk, encoding, cb) {
      if (!this.muted) {
        process.stdout.write(chunk, encoding)
      }
      cb()
    }
  })
  const rl = readline.createInterface({
    input: process.stdin,
    output: mutableStdout,
    terminal: true,
  })
  const password = await new Promise((rs, rj) => {
    rl.question(prompt, (password) => {
      rl.close()
      rs(password)
    })
    mutableStdout.muted = true
  })
  return password
}

// Creates a new wallet
function getWallet() {
  const web3 = new Web3()
  const entropy = crypto.randomBytes(64)
  const { address, privateKey } = web3.eth.accounts.create(entropy)
  const publicKey = secp256k1.publicKeyCreate(toBytes(privateKey.replace('0x', '')))
  return {
    address,
    publicKey: fromBytes(publicKey),
    privateKey,
  }
}
