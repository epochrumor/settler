const secp256k1 = require('secp256k1')

const evenPad = key => key.length % 2 === 0 ? key : `0${key}`
const toBytes = (hex, base = 16) =>
  new Uint8Array(evenPad(hex).match(/.{1,2}/g).map(byte => parseInt(byte, base)))
const fromBytes = (bytes, base = 16) =>
  Array.prototype.map.call(bytes, (byte) => {
    return `0${byte.toString(base)}`.slice(-2)
  }).join('')

function sharedSecret(_privateKey, _publicKey) {
  const hashfn = (x, y) => {
    const pubKey = new Uint8Array(33)
    pubKey[0] = (y[31] & 1) === 0 ? 0x02 : 0x03
    pubKey.set(x, 1)
    return pubKey
  }
  const privateKey = _privateKey.indexOf('0x') === -1 ? _privateKey : _privateKey.replace('0x', '')
  const publicKey = _publicKey.indexOf('0x') === -1 ? _publicKey : _publicKey.replace('0x', '')
  const privateBytes = toBytes(privateKey)
  const publicBytes = toBytes(publicKey)
  return secp256k1.ecdh(publicBytes, privateBytes, { hashfn }, Buffer.alloc(33)).toString('hex')
}

module.exports = {
  sharedSecret,
  toBytes,
  fromBytes,
}
