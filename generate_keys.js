const fs = require('fs')
const path = require('path')
const openpgp = require('openpgp')
const readline = require('readline')
const { Writable } = require('stream')

const outdir = path.join(__dirname, 'keys')
if (fs.existsSync(outdir)) {
  console.log(`Directory ${outdir} aleady exists, refusing to overwrite`)
  process.exit(1)
}

;(async () => {
   // Generate 3 openpgp keypairs. Each should be used for encrypting private keys.
   // Keypairs should be stored in cold locations
  const mutableStdout = new Writable({
    write: function (chunk, encoding, cb) {
      if (!this.muted) {
        process.stdout.write(chunk, encoding)
      }
      cb()
    }
  })
  const rl = readline.createInterface({
    input: process.stdin,
    output: mutableStdout,
    terminal: true,
  })
  // Wait for input
  const password = await new Promise((rs, rj) => {
    rl.question('Enter a master password:\n', (password) => {
      rl.close()
      rs(password)
    })
    mutableStdout.muted = true
  })
  
  fs.mkdirSync(outdir)
  for (let x = 0; x < 3; x++) {
    const { privateKeyArmored, publicKeyArmored } = await openpgp.generateKey({
      userIds: [{ name: 'Chance Hudson', email: 'chance@epochrumor.com' }],
      curve: 'ed25519',
      passphrase: password,
    })
    fs.writeFileSync(path.join(outdir, `accounts${x}.pub`), publicKeyArmored)
    fs.writeFileSync(path.join(outdir, `accounts${x}.key`), privateKeyArmored)
  }
})()
